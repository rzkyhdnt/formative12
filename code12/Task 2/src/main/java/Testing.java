import org.junit.Assert;
import org.junit.Test;
import java.util.Collections;
import java.util.List;

public class Testing {
    @Test
    public void getMoney() {
        Main getMoney = new Main();
        int result = getMoney.getPaid();
        Assert.assertEquals(2250000, result);
    }

    @Test
    public void getMoneyNegative() {
        Main getMoney = new Main();
        int result = getMoney.getPaid();
        Assert.assertEquals(1500000, result);
    }

    @Test
    public void checkWinner() {
        Main checkWinner = new Main();
        checkWinner.getPerson();
        List<Integer> person = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        Collections.sort(checkWinner.getWinner);
        for(int i=0; i<person.size()-1; i++){
            if(person.equals(checkWinner.getWinner.get(i))){
                Assert.assertEquals(person.get(i), checkWinner.getWinner.get(i));
            }
        }
    }

    @Test
    public void checkPaidPerLoop(){
        Main checkPaidPerLoop = new Main();
        checkPaidPerLoop.getPerson();
        int result = checkPaidPerLoop.paidPerLoop;
        Assert.assertEquals(2250000, result);
    }

    @Test
    public void checkPaidPerLoopNegative(){
        Main checkPaidPerLoop = new Main();
        checkPaidPerLoop.getPerson();
        int result = checkPaidPerLoop.paidPerLoop;
        Assert.assertEquals(1000000, result);
    }
}

